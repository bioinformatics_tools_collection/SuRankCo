package de.rki.ng4.surankco.data;

import java.util.Iterator;
import java.util.Vector;

public abstract class Assembly {
	
	protected String contigFile;
	protected String readFile;
	
	protected Vector<Contig> contigs;

	public abstract Vector<Contig> getContigs();

	public abstract Vector<Vector<Integer>> getPooledReadQualities(
			Vector<Vector<String>> readIds);

	public abstract Vector<Vector<Integer>> getReadLengths(Vector<Vector<String>> readIds);

	public void lengthFilter(int minContigSize) {
//		int counter = 0;
		for (Iterator<Contig> iterator = contigs.iterator(); iterator.hasNext(); ){
			if (iterator.next().getLength() < minContigSize){
				iterator.remove();
//				counter++;
			}
		}
//		System.out.println("removed " + counter + " small contigs");
	}
}
