package de.rki.ng4.surankco.data;

import java.util.Vector;

import de.rki.ng4.surankco.data.Assembly;
import de.rki.ng4.surankco.files.Ace;

public class AssemblyAce extends Assembly {

	private FastqReads reads;
	
	public AssemblyAce(String aceFileName, String readFileName, String fastqParameter){
		contigs = new Vector<Contig>();
		
		contigFile = aceFileName;
		readFile = readFileName;
		
		Ace aceIn = new Ace(aceFileName);
		aceIn.readConsensusSequences();
		aceIn.readQualityValues();

		reads = new FastqReads(readFileName, fastqParameter);
		
		convertContigs(aceIn);
	}
	
	/*
	 * convert Ace Contigs & Fastq reads to internal Contigs
	 */
	private void convertContigs(Ace aceIn){
		Contig contig;
		Read read;
		Vector<Read> reads;
		for (AceContig aceContig : aceIn.getContigs()){
			contig = new Contig(aceContig.getId());
			contig.setLength(aceContig.getQualityValues().size());
			contig.setBaseCount(aceContig.getBaseCount());
			contig.setReadCount(aceContig.getReadCount());
			contig.setBaseSeqmentCount(aceContig.getBaseSeqmentCount());
			contig.setConsensusSequence(aceContig.getConsensusSequence());
			contig.setQualityValues(aceContig.getQualityValues());
			
			contig.setNucleotideCount(countNucleotides(aceContig));
			
			reads = new Vector<Read>();
			for (AceRead aceRead : aceContig.getReads()){
				read = new Read(aceRead.getId());
				read.setOrientation(aceRead.getOrientation());
				read.setOffset(aceRead.getOffset());
				read.setPaddedLength(aceRead.getPaddedLength());
//				read.setPaddedSequence(aceRead.getPaddedSequence());
				read.setAlignClippingStart(aceRead.getAlignClippingStart());
				read.setAlignClippingEnd(aceRead.getAlignClippingEnd());
				reads.add(read);
			}
			contig.setReads(reads);
			contigs.add(contig);
		}
	}

	private int[][] countNucleotides(AceContig c){
		int[][] nucleotides = new int[c.getConsensusSequence().length()][6];

		String readSeq;
		int offset, contigPos;
		boolean clipped;
		for (AceRead read : c.getReads()){
			readSeq = read.getPaddedSequence().toUpperCase();
			offset = read.getOffset();
			clipped = false;
			for (int readPos=(read.getAlignClippingStart()-1); 
					readPos<read.getAlignClippingEnd(); readPos++){
				contigPos = readPos + offset - 1;

				// workaround for reads out of contig range
				if ((contigPos < 0) || (contigPos >= nucleotides.length)){
					clipped = true;
				}
				else{
				
				switch (readSeq.charAt(readPos)){
				case 'A':
					nucleotides[contigPos][0]++;
					break;
				case 'C':
					nucleotides[contigPos][1]++;
					break;
				case 'G':
					nucleotides[contigPos][2]++;
					break;
				case 'T':
					nucleotides[contigPos][3]++;
					break;
				case 'N':
					nucleotides[contigPos][4]++;
					break;
				case '*':
					nucleotides[contigPos][5]++;
					break;
				default:
				}
				
				}
			}
			if (clipped) System.out.println("warning: clipped read " + read.getId());
		}

		return nucleotides;
	}
	
	@Override
	public Vector<Contig> getContigs() {
		return contigs;
	}

	@Override
	public Vector<Vector<Integer>> getPooledReadQualities(
			Vector<Vector<String>> readIds) {
		Vector<Vector<Integer>> readQualities = reads.getPooledReadQualities(readIds);
		reads.readMatchCheck(readQualities, getIds(), contigFile, readFile);
		return readQualities;
	}

	@Override
	public Vector<Vector<Integer>> getReadLengths(Vector<Vector<String>> readIds) {
		Vector<Vector<Integer>> readLengths = reads.getReadLengths(readIds);
		reads.readMatchCheck(readLengths, getIds(), contigFile, readFile);
		return readLengths;
	}
	
	public Vector<String> getIds(){
		Vector<String> ids = new Vector<String>();
		for (Contig c : contigs){
			ids.add(c.getId());
		}
		return ids;
	}
	
	@SuppressWarnings("unused")
	private Vector<Vector<Character>> buildAlignment(AceContig c){
		Vector<Vector<Character>> alignment = new Vector<Vector<Character>>();

		for (Character ch : c.getConsensusSequence().toCharArray()){
			alignment.add(new Vector<Character>());
			alignment.lastElement().add(ch);
		}

		int offset;
		int contigPos;

		for (AceRead read : c.getReads()){
			offset = read.getOffset();
			for (int readPos=(read.getAlignClippingStart()-1); 
					readPos<read.getAlignClippingEnd(); readPos++){
				contigPos = readPos + offset - 1;
				alignment.get(contigPos).add(read.getPaddedSequence().charAt(readPos));
			}
		}

		return alignment;
	}
}
