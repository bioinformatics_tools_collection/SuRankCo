package de.rki.ng4.surankco.data;

import htsjdk.samtools.SAMRecord;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import de.rki.ng4.surankco.files.Fasta;
import de.rki.ng4.surankco.files.Sam;

public class AssemblySam extends Assembly {

	public static void main(String[] args){
		String samfile = "data/ex1.sam";
		String fastafile = "data/ex1.fa";
		AssemblySam test = new AssemblySam(fastafile, samfile);
		for (Contig contig : test.getContigs()){
			System.out.println(contig.getId());
		}
	}

	public AssemblySam(String fastaFileName, String samFileName){
		contigs = new Vector<Contig>();
		contigFile = fastaFileName;
		readFile = samFileName;

		Fasta fasta = new Fasta(fastaFileName);
		Vector<FastaContig> fastaContigs = fasta.parseFasta();

		//		for (FastaContig contig : fastaContigs){
		//			System.out.println(contig.getId() + " (length=" + contig.getSequence().length() + ")");
		//			System.out.println(contig.getSequence());
		//		}

		Sam sam = new Sam(samFileName);
		HashMap<String, SamContig> samContigs = sam.getContigs();

		convertContigs(fastaContigs, samContigs);

		//		for (SamContig contig : samContigs.values()){
		//			System.out.println(contig.getId());
		//		}
	}

	/*
	 * convert Fasta Contigs & Sam Reads to internal Contigs
	 */
	private void convertContigs(Vector<FastaContig> fastaContigs, HashMap<String, SamContig> samContigs){
		SamContig samContig;
		Contig contig;
		Read read;
		Vector<Read> reads;
		for (FastaContig fastaContig : fastaContigs){
			if (samContigs.containsKey(fastaContig.getId())){
				samContig = samContigs.get(fastaContig.getId());

				contig = new Contig(fastaContig.getId());

				contig.setLength(fastaContig.getSequence().length());
				contig.setConsensusSequence(fastaContig.getSequence());

				contig.setReadCount(samContig.getReads().size());

				contig.setNucleotideCount(countNucleotides(samContig.getAlignment()));

				//			contig.setBaseCount(fastaContig.getSequence().length());
				//			contig.setBaseSeqmentCount(fastaContig.getBaseSeqmentCount());
				//			contig.setQualityValues(fastaContig.getQualityValues());

				reads = new Vector<Read>();
				for (SAMRecord samRead : samContig.getReads()){
					read = new Read(samRead.getReadName());
					read.setOffset(samRead.getUnclippedStart());
					read.setAlignClippingStart(samRead.getAlignmentStart() - samRead.getUnclippedStart() + 1);
					read.setAlignClippingEnd(samRead.getAlignmentEnd() - samRead.getUnclippedEnd() + 1);
					//				read.setPaddedSequence(samRead.getReadString()); // sam read sequence is not padded

					if (samRead.getReadNegativeStrandFlag()) read.setOrientation('C');
					else read.setOrientation('U');

					read.setQualityValues(parsePhred(samRead.getBaseQualityString()));

					// not sure if correct
					read.setPaddedLength(samRead.getCigar().getReferenceLength());

					reads.add(read);
				}
				contig.setReads(reads);
				contigs.add(contig);
			}
			else {
				System.out.println("warning: skipped contig " + fastaContig.getId() +
						" (no reads in sam file)");
			}	
		}
	}

	private int[][] countNucleotides(ArrayList<ArrayList<Byte>> alignment) {
		int[][] nucleotides = new int[alignment.size()][6];

		for (int i=0; i<alignment.size(); i++){
			for (Byte nucleotide : alignment.get(i)){
				switch ((char) nucleotide.byteValue()){
				case 'A':
					nucleotides[i][0]++;
					break;
				case 'C':
					nucleotides[i][1]++;
					break;
				case 'G':
					nucleotides[i][2]++;
					break;
				case 'T':
					nucleotides[i][3]++;
					break;
				case 'N':
					nucleotides[i][4]++;
					break;
				case '*':
					nucleotides[i][5]++;
					break;
				default:
				}
			}
		}

		return nucleotides;
	}

	private Vector<Integer> parsePhred(String charQualities){
		Vector<Integer> intQualities = new Vector<Integer>();
		for (char qual : charQualities.toCharArray()){
			intQualities.add((int) qual - 33);
		}
		return intQualities;
	}

	@Override
	public Vector<Contig> getContigs() {
		return contigs;
	}

	@Override
	public Vector<Vector<Integer>> getPooledReadQualities(Vector<Vector<String>> readIds) {
		Vector<Vector<Integer>> pooledReadQualities = new Vector<Vector<Integer>>();

		for (Contig contig : contigs){
			Vector<Integer> readQualities = new Vector<Integer>();
			for (Read read : contig.getReads()){
				readQualities.addAll(read.getQualityValues());
			}
			pooledReadQualities.add(readQualities);
		}

		return pooledReadQualities;
	}

	@Override
	public Vector<Vector<Integer>> getReadLengths(Vector<Vector<String>> readIds) {
		Vector<Vector<Integer>> readLengths = new Vector<Vector<Integer>>();

		for (Contig contig : contigs){
			Vector<Integer> lengths = new Vector<Integer>();
			for (Read read : contig.getReads()){
				lengths.add(read.getQualityValues().size());
			}
			readLengths.add(lengths);
		}

		return readLengths;
	}

}
