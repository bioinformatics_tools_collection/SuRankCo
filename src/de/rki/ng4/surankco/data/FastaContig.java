package de.rki.ng4.surankco.data;

public class FastaContig {

	private String id;
	private StringBuffer sequence;
	
	public FastaContig(String header){
		this.id = header.substring(1).split(" ")[0];
		sequence = new StringBuffer();
	}
	
	public void appendSequence(String sequence){
		this.sequence.append(sequence);
	}
	
	public String getSequence(){
		return sequence.toString();
	}

	public String getId() {
		return id;
	}
}
