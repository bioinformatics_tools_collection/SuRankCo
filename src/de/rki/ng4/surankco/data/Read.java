package de.rki.ng4.surankco.data;

import java.util.Vector;

public class Read {

	private String id;
	private char orientation;
	private int offset;
	
	private int paddedLength;
//	private String paddedSequence; // not need anymore, nucleotide counts
	
	private int alignClippingStart;
	private int alignClippingEnd;
	
	private Vector<Integer> qualityValues;
	
	public Read(String id){
		this.id = id;
	}
	
	public Integer getClippingLength() {
		return (alignClippingEnd-alignClippingStart+1);
	}
	
		
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public char getOrientation() {
		return orientation;
	}
	
	public void setOrientation(char orientation) {
		this.orientation = orientation;
	}
	
	public int getOffset() {
		return offset;
	}
	
	public void setOffset(int offset) {
		this.offset = offset;
	}
	
	public int getPaddedLength() {
		return paddedLength;
	}
	
	public void setPaddedLength(int paddedLength) {
		this.paddedLength = paddedLength;
	}
	
//	public String getPaddedSequence() {
//		return paddedSequence;
//	}
	
//	public void setPaddedSequence(String paddedSequence) {
//		this.paddedSequence = paddedSequence;
//	}
	
	public int getAlignClippingStart() {
		return alignClippingStart;
	}
	
	public void setAlignClippingStart(int alignClippingStart) {
		this.alignClippingStart = alignClippingStart;
	}
	
	public int getAlignClippingEnd() {
		return alignClippingEnd;
	}
	
	public void setAlignClippingEnd(int alignClippingEnd) {
		this.alignClippingEnd = alignClippingEnd;
	}

	public Vector<Integer> getQualityValues() {
		return qualityValues;
	}

	public void setQualityValues(Vector<Integer> qualityValues) {
		this.qualityValues = qualityValues;
	}	
}
