/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.surankco.feature;

import java.util.Vector;

import de.rki.ng4.surankco.data.Contig;
import de.rki.ng4.surankco.data.Read;

public class Several {

	private Vector<Contig> contigs;

	public Several(Vector<Contig> contigs){
		this.contigs = contigs;
	}


	public void printContigInfo(){
		for (Contig c : contigs){
			System.out.println(c.getId());
			System.out.println(c.getQualityValues().toString());
		}
	}


	public Vector<String> getIds(){
		Vector<String> ids = new Vector<String>();
		for (Contig c : contigs){
			ids.add(c.getId());
		}
		return ids;
	}

	public Vector<Integer> getBaseCounts(){
		Vector<Integer> baseCounts = new Vector<Integer>();
		for (Contig c : contigs){
			baseCounts.add(c.getBaseCount());
		}
		return baseCounts;
	}

	public Vector<Integer> getReadCounts(){
		Vector<Integer> readCounts = new Vector<Integer>();
		for (Contig c : contigs){
			readCounts.add(c.getReadCount());
		}
		return readCounts;
	}

	public Vector<Integer> getBaseSeqmentCounts(){
		Vector<Integer> baseSeqmentCounts = new Vector<Integer>();
		for (Contig c : contigs){
			baseSeqmentCounts.add(c.getBaseSeqmentCount());
		}
		return baseSeqmentCounts;
	}

	public Vector<Integer> getContigLength(){
		Vector<Integer> length = new Vector<Integer>();
		for (Contig c : contigs){
			length.add(c.getLength());
		}
		return length;
	}

	public Vector<Double> getReadComplementFraction(){
		Vector<Double> fraction = new Vector<Double>();
		double complements;
		for (Contig c : contigs){
			complements = 0;
			for (Read read : c.getReads()){
				if (read.getOrientation() == 'C') complements++;
			}
			fraction.add(complements / (double) c.getReads().size());
		}
		return fraction;
	}

	public Vector<String> getConsensusSequences(){
		Vector<String> conSeqs = new Vector<String>();
		for (Contig c : contigs){
			conSeqs.add(c.getConsensusSequence());
		}
		return conSeqs;
	}

	public Vector<Vector<Integer>> getQualityValues(){
		Vector<Vector<Integer>> qualityValues = new Vector<Vector<Integer>>();
		for (Contig c : contigs){
			qualityValues.add(c.getQualityValues());
		}
		return qualityValues;
	}

	// Collects contig read ids and uses a regex (if indicated) to split off additional suffixes in contrast to the original read names in fastq/qual files
	public Vector<Vector<String>> getReadIds(String regex){
		Vector<Vector<String>> readIds = new Vector<Vector<String>>();
		
		if (regex.matches("nosplit")){
			for (Contig c : contigs){
				readIds.add(new Vector<String>());
				for (Read read : c.getReads()){
					readIds.lastElement().add(read.getId());
				}
			}
		}
		else {
			for (Contig c : contigs){
				readIds.add(new Vector<String>());
				for (Read read : c.getReads()){
					readIds.lastElement().add(read.getId().split(regex)[0]);
				}
			}
		}
		
		return readIds;
	}
	
	public Vector<Vector<Integer>> getReadPaddedLengths(){
		Vector<Vector<Integer>> lengths = new Vector<Vector<Integer>>();
		for (Contig c : contigs){
			lengths.add(new Vector<Integer>());
			for (Read read : c.getReads()){
				lengths.lastElement().add(read.getPaddedLength());
			}
		}
		return lengths;
	}

	public Vector<Vector<Integer>> getReadClippingLengths(){
		Vector<Vector<Integer>> clippings = new Vector<Vector<Integer>>();
		for (Contig c : contigs){
			clippings.add(new Vector<Integer>());
			for (Read read : c.getReads()){
				clippings.lastElement().add(read.getClippingLength());
			}
		}
		return clippings;
	}

	public void printConsensusSequences(){
		for (Contig c : contigs){
			System.out.println(c.getId() + " (l=" + c.getConsensusSequence().length() + "):\t" + c.getConsensusSequence().substring(0,Math.min(100, c.getConsensusSequence().length())) + "...");
		}
	}

	public void printContigReads(int contigNum){
		Contig c = contigs.get(contigNum-1);
		System.out.println("Reads in Contig " + c.getId() + ":");
		for (Read read : c.getReads()){
			System.out.println(read.getId() + ":\t" + read.getOrientation() + " " + read.getOffset());
		}
	}

	public Vector<Vector<Integer>> getGlobalCoverages(){
		Vector<Vector<Integer>> coverages = new Vector<Vector<Integer>>();

		for (Contig c : contigs){
			coverages.add(calcCoverage(c.getConsensusSequence(), c.getNucleotideCount()));
		}

		return coverages;
	}

	public Vector<Vector<Integer>> getCoreCoverages(){
		Vector<Vector<Integer>> ccoverages = new Vector<Vector<Integer>>();

		for (Contig c : contigs){
			ccoverages.add(calcCoreCoverage(c.getConsensusSequence(), c.getNucleotideCount()));
		}

		return ccoverages;
	}

	private Vector<Integer> calcCoverage(String consensus, int[][] nucCounts){
		Vector<Integer> coverage = new Vector<Integer>();
		int nucCountSize = nucCounts.length;
		int consensusSize = consensus.length();
		for (int i=0; i<consensus.length(); i++){
			if (consensus.charAt(i) != '*'){
				coverage.add(nucCounts[i][0] + nucCounts[i][1] + nucCounts[i][2] + 
						nucCounts[i][3] + nucCounts[i][4] + nucCounts[i][5]);
			}
			//else System.out.print("*");
		}
		return coverage;
	}

	//	Supported Nucleotides:
	//	A 	Adenin
	//	C 	Cytosin
	//	G 	Guanin
	//	T 	Thymin
	//	R 	G A (PuRine)
	//	Y 	T C (PYrimidine)
	//	K 	G T (Ketone)
	//	M 	A C (AMinogruppen)
	//	S 	G C (Starke Wechselwirkung)
	//	W 	A T (Weiche Wechselwirkung)
	//	B 	G T C (nicht A) (B kommt nach A)
	//	D 	G A T (nicht C) (D kommt nach C)
	//	H 	A C T (nicht G) (H kommt nach G)
	//	V 	G C A (nicht T, nicht U) (V kommt nach U)
	//	N 	A G C T (aNy)
	private Vector<Integer> calcCoreCoverage(String consensus, int[][] nucCounts){
		Vector<Integer> ccoverage = new Vector<Integer>();
		for (int i=0; i<consensus.length(); i++){
			switch (consensus.charAt(i)){
			case 'A': // A 	Adenin
				ccoverage.add(nucCounts[i][0]);
				break;
			case 'C': // C 	Cytosin
				ccoverage.add(nucCounts[i][1]);
				break;
			case 'G': // G 	Guanin
				ccoverage.add(nucCounts[i][2]);
				break;
			case 'T': // T 	Thymin
				ccoverage.add(nucCounts[i][3]);
				break;
			case 'R': // R 	G A (PuRine)
				ccoverage.add(Math.max(nucCounts[i][2], nucCounts[i][0]));
				break;
			case 'Y': // Y 	T C (PYrimidine)
				ccoverage.add(Math.max(nucCounts[i][3], nucCounts[i][1]));
				break;
			case 'K': // K 	G T (Ketone)
				ccoverage.add(Math.max(nucCounts[i][2], nucCounts[i][3]));
				break;
			case 'M': // M 	A C (AMinogruppen)
				ccoverage.add(Math.max(nucCounts[i][0], nucCounts[i][1]));
				break;		
			case 'S': // S 	G C (Starke Wechselwirkung)
				ccoverage.add(Math.max(nucCounts[i][2], nucCounts[i][1]));
				break;
			case 'W': // W 	A T (Weiche Wechselwirkung)
				ccoverage.add(Math.max(nucCounts[i][0], nucCounts[i][3]));
				break;
			case 'B': // B 	G T C (nicht A) (B kommt nach A)
				ccoverage.add(Math.max(Math.max(nucCounts[i][2], nucCounts[i][3]), nucCounts[i][1]));
				break;
			case 'D': // D 	G A T (nicht C) (D kommt nach C)
				ccoverage.add(Math.max(Math.max(nucCounts[i][2], nucCounts[i][0]), nucCounts[i][3]));
				break;
			case 'H': // H 	A C T (nicht G) (H kommt nach G)
				ccoverage.add(Math.max(Math.max(nucCounts[i][0], nucCounts[i][1]), nucCounts[i][3]));
				break;
			case 'V': // V 	G C A (nicht T, nicht U) (V kommt nach U)
				ccoverage.add(Math.max(Math.max(nucCounts[i][2], nucCounts[i][1]), nucCounts[i][0]));
				break;
			case 'N': // N 	A G C T (aNy)
				ccoverage.add(Math.max(Math.max(nucCounts[i][0], nucCounts[i][1]), 
						Math.max(nucCounts[i][2], nucCounts[i][3])));
				break;
			case '*':
				//System.out.print("*");
				break;
			default:
				ccoverage.add(nucCounts[i][0] + nucCounts[i][1] + nucCounts[i][2] + 
						nucCounts[i][3] + nucCounts[i][4] + nucCounts[i][5]);
				System.out.println("warning: unsupported nucleotide in consensus at position " + i + ": " + consensus.charAt(i) + " -> using full coverage for this position");
			}
		}
		return ccoverage;
	}

	public Vector<Double> getGcContent(){
		Vector<Double> content = new Vector<Double>();
		double gc, at; 
		for (Contig c : contigs){
			at = 0;
			gc = 0;
			for (Character ch : c.getConsensusSequence().toUpperCase().toCharArray()){
				if (ch == 'A' || ch == 'T') at++;
				if (ch == 'G' || ch == 'C') gc++;
			}
			content.add(gc/(gc+at));
		}
		return content;
	}


}
