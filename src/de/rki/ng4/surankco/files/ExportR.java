/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.surankco.files;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Vector;

public class ExportR {

	private Vector<String> header;
	private Vector<Vector<String>> data;

	public ExportR(){
		header = new Vector<String>();
		data = new Vector<Vector<String>>();
	}

	public void addStringVariable(String name, Vector<String> values){
		header.add(name);
		data.add(values);
	}
	
	public void addIntegerVariable(String name, Vector<Integer> values){
		header.add(name);
		data.add(ints(values));
	}
	
	public void addDoubleVariable(String name, Vector<Double> values){
		header.add(name);
		data.add(doubles(values));
	}

	public void write(String fileName){
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(fileName)));
			try {
				for (String s : header){
					bw.append(s + " ");
				}
				bw.newLine();				

				for (int i=0; i<data.firstElement().size(); i++){
					for (int j=0; j<data.size(); j++){
						bw.append(data.get(j).get(i) + " ");
					}
					bw.newLine();
				}
			}
			finally {
				bw.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("error: problems writing file: " + fileName);
			System.exit(1);
		}
	}

	public static Vector<String> intVecAsRString(Vector<Vector<Integer>> vecs){
		Vector<String> output = new Vector<String>();
		StringBuffer string;
		for (Vector<Integer> v : vecs){
			string = new StringBuffer("\"");
			for (Integer i : v){
				string.append(i + " ");
			}
			string.replace(string.length(), string.length(), "\"");
			output.add(string.toString());
		}
		return output;
	}
	
	private static Vector<String> ints(Vector<Integer> vecs){
		Vector<String> output = new Vector<String>();
		for (Integer i : vecs){
			output.add(i.toString());
		}
		return output;
	}
	
	private static Vector<String> doubles(Vector<Double> vecs){
		Vector<String> output = new Vector<String>();
		for (Double i : vecs){
			output.add(i.toString());
		}
		return output;
	}

	public void writeCoverage(String fileName, Vector<String> ids, Vector<Vector<Integer>> covs) {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(fileName)));
			try {
				DecimalFormat df =   new DecimalFormat( "000" );
				
				for (int i=0; i<ids.size(); i++){
					bw.append(">" + ids.get(i));
					bw.newLine();
					
					
					
					int count = 0;
					for (int j=0; j<covs.get(i).size(); j++){
						
						bw.append( df.format(covs.get(i).get(j)) + " " );
						count++;
						
						if (count == 15){
							bw.newLine();
							count = 0;
						}
					}
					bw.newLine();
				}
			}
			finally {
				bw.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("error: problems writing file: " + fileName);
			System.exit(1);
		}
	}
}
