package de.rki.ng4.surankco.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import de.rki.ng4.surankco.data.FastaContig;

public class Fasta {

	private String filename;
	
	public Fasta(String filename){
		this.filename = filename;
	}
	
	public Vector<FastaContig> parseFasta(){
		Vector<FastaContig> contigs = new Vector<FastaContig>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(filename)));
			try {
				String line;
				while ((line = br.readLine()) != null){
					if (line.startsWith(">")){
						contigs.add(new FastaContig(line));
					}
					else {
						contigs.lastElement().appendSequence(line.trim().toUpperCase());
					}
				}
			}
			finally {
				br.close();
			}
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("error: problems reading file: " + filename);
			System.exit(1);
		} 
		catch (IOException e) {
			e.printStackTrace();
			System.out.println("error: problems reading file: " + filename);
			System.exit(1);
		}
		return contigs;
	}
	
}
