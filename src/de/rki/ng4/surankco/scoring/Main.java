/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.surankco.scoring;

public class Main {

	// debugging
	public final static boolean DEBUGMODE = false;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String filename = args[0];
		String outputFile = args[1];
				
		if (Main.DEBUGMODE){ System.out.println("parse pretty psl file..."); }
		PSLPrettyParser psl = new PSLPrettyParser(filename);
		
		if (Main.DEBUGMODE){ System.out.println("calculate metrics..."); }
		MetricsPSL metrics = new MetricsPSL(psl.getHits());
		metrics.calculateMetrics();
		if (Main.DEBUGMODE){ System.out.println("export metrics..."); }
		metrics.export(outputFile);

	}

}
