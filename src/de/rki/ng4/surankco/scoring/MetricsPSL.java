/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.surankco.scoring;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

import de.rki.ng4.surankco.scoring.scores.NormedContigLength1;
import de.rki.ng4.surankco.scoring.scores.NormedContigLength2;
import de.rki.ng4.surankco.scoring.scores.Score;


public class MetricsPSL {
		
	private Vector<PSLHit> hits;
	private Vector<Vector<Boolean>> matchSequences;
	
	private Vector<String> metricsHeader;
	private Vector<Vector<Double>> metricsValues;
	
	public MetricsPSL(Vector<PSLHit> hits){
		this.hits = hits;
		
		matchSequences = new Vector<Vector<Boolean>>();
		for (PSLHit hit : hits){
			matchSequences.add(hit.getExtendedMatchSequence());
		}
		
		metricsHeader = new Vector<String>();
		metricsValues = new Vector<Vector<Double>>();
	}
		
	public void calculateMetrics(){
		Vector<Double> matchCounts = matchCount();
		Vector<Double> errorCounts = errorCount(matchCounts);
		Vector<Double> errorSubtractions = errorSubtraction(errorCounts);
		
		Vector<Double> normMatchCounts1 = normMatchCount1(matchCounts);
		Vector<Double> normMatchCounts2 = normMatchCount2(matchCounts);
		Vector<Double> normErrorCounts1 = normErrorCount1(errorCounts);
		Vector<Double> normErrorCounts2 = normErrorCount2(errorCounts);
		Vector<Double> normErrorSubtractions = normErrorSubtraction(errorSubtractions);
		
		Vector<Double> maxContErrors = maxContError();
		Vector<Double> maxRegionErrors = maxRegionError(100);
		
		Vector<Double> maxEndErrorStretches = maxEndErrorStretch();
		Vector<Double> maxEndErrorCounts = maxEndErrorCount(100);
		Vector<Double> maxEndContErrors = maxEndContError(100);
			
		metricsValues.add(matchCounts);
		metricsValues.add(errorCounts);
		metricsValues.add(errorSubtractions);
		
		metricsValues.add(normMatchCounts1);
		metricsValues.add(normMatchCounts2);
		metricsValues.add(normErrorCounts1);
		metricsValues.add(normErrorCounts2);
		metricsValues.add(normErrorSubtractions);
		
		metricsValues.add(maxContErrors);
		metricsValues.add(maxRegionErrors);
		
		metricsValues.add(maxEndErrorStretches);
		metricsValues.add(maxEndErrorCounts);
		metricsValues.add(maxEndContErrors);
		
		
		// New Scoring implementation
		Vector<Score> scores = new Vector<Score>();
		scores.add(new NormedContigLength1());
		scores.add(new NormedContigLength2());
		
		Vector<Vector<Double>> results = new Vector<Vector<Double>>();
		for (Score score : scores){
			metricsHeader.add(score.getScoreName());
			results.add(new Vector<Double>());
		}
		
		for (PSLHit hit : hits){
			for (int i=0; i<scores.size(); i++){
				results.get(i).add(scores.get(i).calculate(hit));
			}
		}		

		for (Vector<Double> result : results){
			metricsValues.add(result);
		}
		
	}
	
	public void printMetrics(){
		System.out.print("ContigID ");
		for (String header : metricsHeader){
			System.out.print(header + " ");
		}
		System.out.println();
		
		for (int i=0; i<hits.size(); i++){
			System.out.print(hits.get(i).getQueryName() + " ");
			for (int j=0; j<metricsValues.size(); j++){
				System.out.print(metricsValues.get(j).get(i) + " ");
			}
			System.out.println();
		}
	}
	
	public void export(String fileName){
		try {
			BufferedWriter mbw = new BufferedWriter(new FileWriter(new File(fileName)));
			
			try {
				mbw.append("ContigID ");
				for (String header : metricsHeader){
					mbw.append(header + " ");
				}
				mbw.newLine();
				
				for (int i=0; i<hits.size(); i++){
					mbw.append(hits.get(i).getQueryName() + " ");
					for (int j=0; j<metricsValues.size(); j++){
						mbw.append(metricsValues.get(j).get(i) + " ");
					}
					mbw.newLine();
				}
			}
			finally {
				mbw.close();
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private Vector<Double> matchCount(){
		metricsHeader.add("MatchCount");
		Vector<Double> values = new Vector<Double>();
		double count;
		for (Vector<Boolean> matchSeq : matchSequences){
			count = 0;
			for (Boolean match : matchSeq){
				if (match) count++;
			}
			values.add(count);
		}
		return(values);
	}
	
	private Vector<Double> errorCount(Vector<Double> matchCounts){
		metricsHeader.add("ErrorCount");
		Vector<Double> values = new Vector<Double>();
		for (int i=0; i<hits.size(); i++){
			values.add(matchSequences.get(i).size() - matchCounts.get(i));
		}
		return(values);
	}
	
	private Vector<Double> errorSubtraction(Vector<Double> errorCounts){
		metricsHeader.add("ErrorSubtraction");
		Vector<Double> values = new Vector<Double>();
		for (int i=0; i<hits.size(); i++){
			values.add( Math.max(0, (hits.get(i).getQuerySize() - errorCounts.get(i))) ); // set to 0 if error count > contig size, e.g. big gaps
		}
		return(values);
	}
	
	private Vector<Double> normMatchCount1(Vector<Double> matchCounts){
		metricsHeader.add("NormedMatchCount1");
		Vector<Double> values = new Vector<Double>();
		for (int i=0; i<hits.size(); i++){
			values.add(matchCounts.get(i) / hits.get(i).getQuerySize());
		}
		return(values);
	}
	
	private Vector<Double> normMatchCount2(Vector<Double> matchCounts){
		metricsHeader.add("NormedMatchCount2");
		Vector<Double> values = new Vector<Double>();
		for (int i=0; i<hits.size(); i++){
			values.add(matchCounts.get(i) / matchSequences.get(i).size());
		}
		return(values);
	}
	
	private Vector<Double> normErrorCount1(Vector<Double> errorCounts){
		metricsHeader.add("NormedErrorCount1");
		Vector<Double> values = new Vector<Double>();
		for (int i=0; i<hits.size(); i++){
			values.add( Math.min(1, (errorCounts.get(i) / hits.get(i).getQuerySize())) ); // set to 1 if error count > contig size, e.g. big gaps
		}
		return(values);
	}
	
	private Vector<Double> normErrorCount2(Vector<Double> errorCounts){
		metricsHeader.add("NormedErrorCount2");
		Vector<Double> values = new Vector<Double>();
		for (int i=0; i<hits.size(); i++){
			values.add(errorCounts.get(i) / matchSequences.get(i).size());
		}
		return(values);
	}
	
	private Vector<Double> normErrorSubtraction(Vector<Double> errorSubtractions){
		metricsHeader.add("NormedErrorSubtraction");
		Vector<Double> values = new Vector<Double>();
		for (int i=0; i<hits.size(); i++){
			values.add( Math.max(0, (errorSubtractions.get(i) / hits.get(i).getQuerySize())) );  // set to 0 if error count > contig size, e.g. big gaps
		}
		return(values);
	}
	
	private Vector<Double> maxContError(){
		metricsHeader.add("MaxContiguousError");
		Vector<Double> values = new Vector<Double>();
		double max, count;
		for (int i=0; i<hits.size(); i++){
			max = 0;
			count = 0;
			for (Boolean b : matchSequences.get(i)){
				if (b){
					max = Math.max(max, count);
					count = 0;
				}
				else count++;
			}
			max = Math.max(max, count);
			values.add( Math.min(1, (max / hits.get(i).getQuerySize())) ); // normed by contig size and set to 1 if error count > contig size, e.g. very big gaps
		}
		return(values);
	}
	
	private Vector<Double> maxRegionError(int regionSize){
		metricsHeader.add("MaxRegionError");
		Vector<Double> values = new Vector<Double>();
		double max, count;
		Vector<Boolean> matchSeq;
		for (int i=0; i<hits.size(); i++){
			max = 0;
			count = 0;
			matchSeq = matchSequences.get(i);
			for (int j=0; j<Math.min(regionSize,matchSeq.size()); j++){
				if (!matchSeq.get(j)) count++;
				max = count;
			}
			for (int j=Math.min(regionSize, matchSeq.size()); j<matchSeq.size(); j++){
				if (!matchSeq.get(j-regionSize)) count--;
				if (!matchSeq.get(j)) count++;
				max = Math.max(max, count);
			}
			values.add(max);
		}
		return(values);
	}
	
	
	private Vector<Double> maxEndErrorStretch(){
		metricsHeader.add("MaxEndErrorStretch");
		Vector<Double> values = new Vector<Double>();
		double left, right;
		int index;
		Vector<Boolean> matchSeq;
		for (int i=0; i<hits.size(); i++){
			left = 0;
			right = 0;
			matchSeq = matchSequences.get(i);
			
			// left error stretch size
			index = 0;
			while (!matchSeq.get(index++)){
				left++;
			}
			
			// right error stretch size
			index = matchSeq.size()-1;
			while (!matchSeq.get(index--)){
				right++;
			}
			
			values.add( Math.max(left, right) / hits.get(i).getQuerySize() ); // normed by contig size
		}
		return(values);
	}
	
	private Vector<Double> maxEndErrorCount(int endSize){
		metricsHeader.add("MaxEndErrorCount");
		Vector<Double> values = new Vector<Double>();
		double left, right;
		Vector<Boolean> matchSeq;
		for (int i=0; i<hits.size(); i++){
			left = 0;
			right = 0;
			matchSeq = matchSequences.get(i);
			
			for (int j=0; j<Math.min(endSize,matchSeq.size()); j++){
				if (!matchSeq.get(j)) left++;
			}
			
			for (int j=matchSeq.size()-1; j>=Math.max(matchSeq.size()-endSize,0); j--){
				if (!matchSeq.get(j)) right++;
			}
			
			values.add(Math.max(left, right));
		}
		return(values);
	}
	
	private Vector<Double> maxEndContError(int endSize){
		metricsHeader.add("MaxEndContError");
		Vector<Double> values = new Vector<Double>();
		double left, right, count;
		Vector<Boolean> matchSeq;
		for (int i=0; i<hits.size(); i++){
			left = 0;
			right = 0;
			matchSeq = matchSequences.get(i);
			
			count = 0;
			for (int j=0; j<Math.min(endSize,matchSeq.size()); j++){
				if (matchSeq.get(j)){
					left = Math.max(left, count);
					count = 0;
				}
				else count++;
			}
			left = Math.max(left, count);
			
			count = 0;
			for (int j=matchSeq.size()-1; j>=Math.max(matchSeq.size()-endSize,0); j--){
				if (matchSeq.get(j)){
					right = Math.max(right, count);
					count = 0;
				}
				else count++;
			}
			right = Math.max(right, count);
			
			values.add(Math.max(left, right));
		}
		return(values);
	}

}
